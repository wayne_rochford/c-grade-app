﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Grades.Test.Types
{
    [TestClass]
    public class RefTypesTests
    {
        [TestMethod]
        public void VariablesHoldAReference()
        {
            GradeBook g1 = new GradeBook();
            GradeBook g2 = g1;

            g1.Name = "Wayne's grade book";
            Assert.AreEqual(g1.Name, g2.Name);

        }

        [TestMethod]
        public void StringComparsion()
        {
            string name1 = "Wayne";
            string name2 = "wayne";

            bool result = string.Equals(name1, name2, StringComparison.InvariantCultureIgnoreCase);
            Assert.IsTrue(result);

        }

        [TestMethod]
        public void VariablesHoldAReferenceX()
        {
            GradeBook g1 = new GradeBook();
            GradeBook g2 = g1;
            
            g1 = new GradeBook();

            g1.Name = "Wayne's grade book";
            Assert.AreEqual(g1.Name, g2.Name);

        }

        [TestMethod]
        public void VariablesHoldAReference2()
        {
            GradeBook g1 = new GradeBook();
            GradeBook g2 = new GradeBook();

            g1.Name = "Wayne's grade book";
            Assert.AreEqual(g1.Name, g2.Name);

        }

        [TestMethod]
        public void IntVariablesHoldAValue()
        {
            int x1 = 100;
            int x2 = x1;

            x1 = 4;
            Assert.AreEqual(x1, x2);

        }
    }
}
